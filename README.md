# Capistrano Docker Deployment strategy

This gem allows you to deploy a pre-built docker image of a rails app and associated dependendent images.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'capistrano-docker-deploy', require: false
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install capistrano-docker-deploy

## Usage

It is assumed that capistrano is already set up on the project, and that a docker repository exists that holds images for deployment (and that it can be accessed)

<!-- You will need to set `:scm` to `nil` as we don't use version control for deployment with this strategy:

```ruby
set :scm, nil
``` -->

### Options

| Name | Default Value | Type | Description |
|------|---------------|------|-------------|
| `:database_image` | `nil` | `String` | Image to use for the database. Set to `nil` for external database server |
| `:docker_image_tag` | `latest` | `String` | Docker tag to deploy |
| `:docker_labels` | `[]` | `Array[String]` | Labels to apply to the docker container (eg for Traefik) |
| `:docker_network` | `nil` | `String` | Network to attach docker container to |
| `:docker_options` | `--log-opt max-size=200m` | `String` | Additional docker options to start containers with |
| `:docker_registry` | `nil` | `String` | Docker registry from which to pull your image (if not the Docker Hub) |
| `:docker_registry_user` | `nil` | `String` | Docker registry username (for `:docker_registry`) |
| `:docker_registry_password` | `nil` | `String` | Docker registry password (for `:docker_registry`) |
| `:docker_registry_image` | `nil` | `String` | Docker registry image |
| `:database_url` | `"postgres://postgres@#{fetch(:slug)}-db/#{fetch(:application)}"` | `String` | Database URL |
| `:environment_variables` | `[]` | `Array[String]` | Environment variables to pass through from the deployer to the docker container |
| `:exposed_ports` | `[]` | `Array[String]` | Ports for docker to expose |
| `:slug` | `"#{fetch(:application)}-#{fetch(:stage)}"` | `String` | Name for the docker container |

### Additional configuration settings

The `server` method can take two further parameters:

| Name | Description |
|------|-------------|
| `reset_on_deploy` | Perform a `rake db:reset` on deployment |
| `migrate_on_deploy` | Perform a `rake db:migrate` on deployment |

To ensure migrations run once and only once, set one server running the Rails application as having the `db` role and set `primary: true` on it.

### To use with Gitlab CI:

Set the following options in `config/deploy.rb`:

```ruby
set :docker_registry, ENV.fetch('CI_REGISTRY')
set :docker_registry_user, ENV.fetch('CI_REGISTRY_USER')
set :docker_registry_password, ENV.fetch('CI_REGISTRY_PASSWORD')
set :docker_registry_image, ENV.fetch('CI_REGISTRY_IMAGE')
```

For other CD/CI environments, refer to their specific documentation.

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on Gitlab at https://gitlab.com/initforthe/capistrano-docker-deploy. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [Contributor Covenant](http://contributor-covenant.org) code of conduct.

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).

## Code of Conduct

Everyone interacting in the Capistrano::Docker::Deploy project’s codebases, issue trackers, chat rooms and mailing lists is expected to follow the [code of conduct](https://gitlab.com/initforthe/capistrano-docker-deploy/blob/master/CODE_OF_CONDUCT.md).
