# frozen_string_literal: true

namespace :deploy do
  task :docker do
    %w[starting updating publishing].each do |task|
      invoke "docker:#{task}"
    end
  end
end
