# frozen_string_literal: true

namespace :docker do
  task :starting do
    invoke 'docker:login'
  end

  task :updating do
    invoke 'docker:pull'
  end

  task :publishing do
    invoke 'docker:app:stop'
    invoke 'docker:db:start'
    invoke 'docker:app:start'
    invoke 'docker:db:setup'
  end

  task :terminate do
    invoke 'docker:app:terminate'
    invoke 'docker:db:terminate'
  end
end
