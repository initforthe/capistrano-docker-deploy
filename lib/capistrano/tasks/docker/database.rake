# frozen_string_literal: true

namespace :docker do
  namespace :db do
    desc 'Attempt to start the database container'
    task :start do
      on roles(:db_server) do
        network_list = [fetch(:docker_db_server_network, nil)].compact.flatten
        networks = network_list.map do |network|
          ['--network', network]
        end.compact.flatten.join(' ')
        execute 'docker', 'run', '-d', '--restart', 'always',
                '--name',
                "#{fetch(:docker_name_prefix)}#{fetch(:db_server_suffix)}",
                fetch(:docker_db_server_options, nil),
                networks, fetch(:database_image), '||', 'true'
      end
    end

    task :stop do
      on roles(:db_server), in: :parallel do
        %w[stop rm].each do |command|
          execute 'docker', command,
                  "#{fetch(:docker_name_prefix)}#{fetch(:db_server_suffix)}",
                  '||', 'true'
        end
      end
    end

    task :terminate do
      invoke 'docker:db:stop'
      invoke 'docker:db:clean'
    end

    task :clean do
      on roles(:db_server), in: :parallel do
        execute 'docker', 'rmi', fetch(:database_image), '||', 'true'
      end
    end

    task :setup do
      invoke 'docker:db:create'
      invoke 'docker:db:migrate'
      invoke 'docker:db:reset'
      invoke 'docker:db:seed'
    end

    task :create do
      on roles(:db, select: :primary, createdb_on_deploy: true) do
        execute 'docker', 'run', '--rm',
                fetch(:db_docker_options),
                primary_network(:db), env_vars(:app), volume_mounts(:app),
                redact(secret_env_vars(:app)),
                "#{fetch(:docker_registry_image)}:#{fetch(:docker_image_tag)}",
                'bin/rails', 'db:create'
      end
    end

    task :migrate do
      on roles(:db, select: :primary, migrate_on_deploy: true) do
        execute 'docker', 'run', '--rm',
                fetch(:db_docker_options),
                primary_network(:db), labels(:app), env_vars(:app), volume_mounts(:app),
                redact(secret_env_vars(:app)),
                "#{fetch(:docker_registry_image)}:#{fetch(:docker_image_tag)}",
                'bin/rails', 'db:migrate'
      end
    end

    task :reset do
      on roles(:db, select: :primary, reset_on_deploy: true) do |_host|
        execute 'docker', 'run', '--rm',
                fetch(:db_docker_options),
                primary_network(:db), labels(:app), env_vars(:app), volume_mounts(:app),
                redact(secret_env_vars(:app)),
                "#{fetch(:docker_registry_image)}:#{fetch(:docker_image_tag)}",
                'bin/rails', 'db:reset'
      end
    end

    task :seed do
      on roles(:db, select: :primary, seed_on_deploy: true) do
        execute 'docker', 'run', '--rm',
                fetch(:db_docker_options),
                primary_network(:db), labels(:app), env_vars(:app), volume_mounts(:app),
                redact(secret_env_vars(:app)),
                "#{fetch(:docker_registry_image)}:#{fetch(:docker_image_tag)}",
                'bin/rails', 'db:seed'
      end
    end
  end
end
