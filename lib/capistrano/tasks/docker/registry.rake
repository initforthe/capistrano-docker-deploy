# frozen_string_literal: true

namespace :docker do
  desc 'Log in to the CI docker registry'
  task :login do
    return unless fetch(:docker_registry, nil)

    on roles(%i[app worker]), in: :parallel do
      execute 'echo', redact(fetch(:docker_registry_password).dup), '|',
              'docker', 'login',
              '-u', fetch(:docker_registry_user), '--password-stdin',
              fetch(:docker_registry)
    end
  end

  desc 'Pull the docker image'
  task :pull do
    on roles(%i[app worker]), in: :parallel do
      execute 'docker', 'pull',
              "#{fetch(:docker_registry_image)}:#{fetch(:docker_image_tag)}"
    end
  end
end
