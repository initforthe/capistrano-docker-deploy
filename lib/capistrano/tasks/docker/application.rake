# frozen_string_literal: true

require 'capistrano/docker/deploy/helpers'
include Capistrano::Docker::Deploy::Helpers # rubocop:disable Style/MixinUsage

namespace :docker do
  namespace :app do
    task :start do
      on roles(:app), in: :parallel do
        execute 'docker', 'run', '-d',
                '--name', "#{fetch(:docker_name_prefix)}#{fetch(:app_suffix)}",
                fetch(:app_docker_options), exposed_ports(:app), volume_mounts(:app),
                primary_network(:app), labels(:app), env_vars(:app),
                redact(secret_env_vars(:app)),
                "#{fetch(:docker_registry_image)}:#{fetch(:docker_image_tag)}",
                fetch(:app_entrypoint, '')

        secondary_networks(:app)&.each do |network|
          execute 'docker', 'network', 'connect', network,
                  "#{fetch(:docker_name_prefix)}#{fetch(:app_suffix)}"
        end
      end

      on roles(:worker), in: :parallel do
        execute 'docker', 'run', '-d', '--restart', 'always',
                '--name',
                "#{fetch(:docker_name_prefix)}#{fetch(:worker_suffix)}",
                fetch(:worker_docker_options), exposed_ports(:worker), volume_mounts(:worker),
                primary_network(:worker), labels(:worker), env_vars(:worker),
                redact(secret_env_vars(:worker)),
                "#{fetch(:docker_registry_image)}:#{fetch(:docker_image_tag)}",
                fetch(:worker_entrypoint, '')

        secondary_networks(:worker)&.each do |network|
          execute 'docker', 'network', 'connect', network,
                  "#{fetch(:docker_name_prefix)}#{fetch(:worker_suffix)}"
        end
      end
    end

    task :stop do
      on roles(:app), in: :parallel do
        %w[stop rm].each do |command|
          execute 'docker', command,
                  "#{fetch(:docker_name_prefix)}#{fetch(:app_suffix)}",
                  '||', 'true'
        end
      end

      on roles(:worker), in: :parallel do
        %w[stop rm].each do |command|
          execute 'docker', command,
                  "#{fetch(:docker_name_prefix)}#{fetch(:worker_suffix)}",
                  '||', 'true'
        end
      end
    end

    task :terminate do
      invoke 'docker:app:stop'
      invoke 'docker:app:clean'
    end

    task :clean do
      # remove the database
      if ENV['DESTROY_DATABASE']
        on roles(:app, select: :primary) do
          execute 'docker', 'run', '--rm',
                  database_network(:app), env_vars(:app),
                  redact(secret_env_vars(:app)),
                  '-e DISABLE_DATABASE_ENVIRONMENT_CHECK=1',
                  "#{fetch(:docker_registry_image)}:#{fetch(:docker_image_tag)}",
                  'bin/rails', 'db:drop'
        end
      end

      on roles(:app) do
        execute 'docker', 'rmi',
                "#{fetch(:docker_registry_image)}:#{fetch(:docker_image_tag)}",
                '||', 'true'
      end

      on roles(:worker) do
        execute 'docker', 'rmi',
                "#{fetch(:docker_registry_image)}:#{fetch(:docker_image_tag)}",
                '||', 'true'
      end
    end
  end
end
