# frozen_string_literal: true

namespace :load do
  task :defaults do
    set :scm, nil

    set :app_roles, fetch(:app_roles, [:app])
    set :worker_roles, fetch(:worker_roles, [:worker])
    set :db_roles, fetch(:db_roles, [:db])
    set :db_server_roles, fetch(:db_server_roles, [:db_server])

    set :app_suffix, fetch(:app_suffix, '')
    set :db_server_suffix, fetch(:db_suffix, '-db')
    set :worker_suffix, fetch(:worker_suffix, '-worker')

    set :app_docker_options, fetch(
      :app_docker_options, '--log-opt max-size=200m'
    )
    set :worker_docker_options, fetch(
      :app_docker_options, '--log-opt max-size=200m'
    )
    set :db_docker_options, fetch(
      :app_docker_options, '--log-opt max-size=200m'
    )
    set :db_server_docker_options, fetch(
      :app_docker_options, '--log-opt max-size=200m'
    )

    # set :scm, nil
    # set :exposed_ports, []
    # set :environment_variables, []
    # set :secrets, []
    # set :environment_variable_map, {}
    # set :database_url, nil
    # set :database_host, nil
    # set :database_port, nil
    # set :database_user, nil
    # set :database_name, nil
    # set :database_image, nil
    # set :docker_image_tag, 'latest'
    # set :docker_labels, []
    # set :docker_options, '--log-opt max-size=200m'
    # set :docker_db_options, '--log-opt max-size=200m'
    # set :docker_networks, []
    # set :docker_registry, nil
    # set :docker_registry_user, nil
    # set :docker_registry_password, nil
    # set :docker_registry_image, nil
    # set :slug, "#{fetch(:application)}-#{fetch(:stage)}"
  end
end
