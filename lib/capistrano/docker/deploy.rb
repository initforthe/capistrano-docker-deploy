# frozen_string_literal: true

load File.expand_path('../tasks/docker/default.rake', __dir__)
load File.expand_path('../tasks/docker/registry.rake', __dir__)
load File.expand_path('../tasks/docker/application.rake', __dir__)
load File.expand_path('../tasks/docker/database.rake', __dir__)

load File.expand_path('../tasks/docker.rake', __dir__)
load File.expand_path('../tasks/deploy.rake', __dir__)
