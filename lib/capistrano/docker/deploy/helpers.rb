# frozen_string_literal: true

module Capistrano
  module Docker
    module Deploy
      # Capistrano deploy helpers to fetch environment variables
      module Helpers
        def env_vars(role)
          vars = []
          vars << env_var_list(fetch(:"#{role}_env_vars", []))
          vars << fetch(:"#{role}_mapped_vars", {}).map do |key, value|
            "-e \"#{key}=#{value}\""
          end.join(' ')
          vars.flatten.join(' ')
        end

        def secret_env_vars(role)
          env_var_list(fetch(:"#{role}_secret_vars", []))
        end

        def labels(role)
          fetch(:"#{role}_docker_labels", [])
            .map { |label| "--label '#{label}'" }.join(' ')
        end

        def exposed_ports(role)
          fetch(:"#{role}_docker_exposed_ports", []).map { |port| "-p #{port}" }
                                                    .join(' ')
        end

        def volume_mounts(role)
          fetch(:"#{role}_volume_mounts", []).map { |mount| "-v #{mount}" }.join(' ')
        end

        def primary_network(role)
          net = fetch(:"#{role}_docker_network", nil)
          return '' if net.nil?

          "--net #{net}"
        end

        def database_network(role)
          net = fetch(
            :"#{role}_database_docker_network",
            fetch(:"#{role}_docker_network", nil)
          )
          return '' if net.nil?

          "--net #{net}"
        end

        def secondary_networks(role)
          [*fetch(:"#{role}_docker_extra_networks", nil)].compact
        end

        private

        def env_var_list(var_set)
          var_set.map do |var|
            value = ENV.fetch(var, nil)
            "-e '#{var}=#{value}'" if value
          end.join(' ')
        end
      end
    end
  end
end
