# frozen_string_literal: true

module Capistrano
  module Docker
    module Deploy
      VERSION = '0.4.0'
    end
  end
end
